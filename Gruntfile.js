module.exports = function(grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
        usemintPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'styles',
                    src: ['*.scss'],
                    dest: 'styles',
                    ext: '.css'
                }]
            }
        },
        watch :{
            files: ['styles/*.scss'],
            task: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: {//browser files
                    src: [
                        'styles/*.scss',
                        '*.html',
                        'js/*.js'
                    ]

                },
                options:{
                    watchTask: true,
                    server:{
                        basedir: './'
                    }
                }
            }
        },
        imagemin: {
            dynamic:{
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'imagenes/*.{png,gif,jpg,jpge}',
                    dest : 'dist/'
                }]

            }
        },

        copy:{
            html: {
                files: [{
                    expand:true,
                    dot:true,
                    cwd: './',
                    src: ['*.html'],
                    dest : 'dist'
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        },
        clean:{
            build:{
                src:['dist/']
            }
        },
        cssmin:{
            dist: {}
        },
        uglify:{
            dist: {}
        },
        filerev:{
            options:{
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release:{
                files: [{
                    src:[
                        'dist/js/*.js',
                        'dist/styles/*.scss'
                    ]
                }]
            }
        },
        concat:{
            options:{
                separator: ';'
            },
            dist: {}
        },

        useminPrepare:{
            foo: {
                dest: 'dist',
                src: ['index.html','about.html','casos.html','contacto.html','precios.html']
            },
            options: {
                flow: {
                    steps:{
                        css: ['cssmin'],
                        js: [uglify]

                    },
                    post:{
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block){
                                var generated = context.options.generated;
                                generated.options={
                                    keepSpecialComents: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html','dist/about.html','dist/casos.html','dist/contacto.html','dist/precios.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        }



    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch']);
    grunt.registerTask('img:compress',['imagemin']);
    grunt.registerTask('build',[
        'clean',
        'copy',
        'imagemin',
        'useminPepare',
        'concat',
        'cssmin',        
        'filerev',
        'usemin',
        'uglify',
    ]);

};