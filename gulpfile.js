'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browsersync = require('browser-sync');    
    
gulp.task('sass', function(){
    return gulp.src('./styles/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./styles'));
});

gulp.task('sass:watch', function(){
    gulp.watch('./styles/*.scss',['sass']);

});



gulp.task('browser-sync', function() {
    var files = ['./*.html','./styles/*.css', './js/*.js', './imagenes/*.{png, jpg, gif}'];
    browserSync.init(files, {
        server: {
            baseDir: "./"
        }
    });
});



gulp.task('default', gulp.series('browser-sync', function() { 
    // default task code here
    gulp.start('sass:watch');
}));


